#ifndef _UTILS__H_
#define _UTILS__H_

#include "ui.h"
#include "ff.h"

typedef struct
{
	lv_point_t line_points[5];
	lv_obj_t* line;
	lv_style_t style;
	lv_obj_t* textlabel;
} ui_AIBoxes_t;

typedef struct
{
    char *str;
    uint16_t color;
} class_label_t;


FRESULT loadModelFile(char* modelFileName, uint8_t ** modelData);
void uiDrawAIBox(ui_AIBoxes_t *box, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint16_t color, const char* textCaption);

#endif /*_UTILS__H_*/