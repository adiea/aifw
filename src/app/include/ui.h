/*
Project: gitlab.com/adiea/aifw
License: MIT
*/
#ifndef UI_H_
#define UI_H_

#include "lvgl.h"

#define UI_SCREEN_MAIN 0

void uiInit(void);

#endif /*UI_H_*/