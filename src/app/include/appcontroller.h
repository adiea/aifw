#ifndef _APP_CONTROLLER_H_
#define _APP_CONTROLLER_H_

typedef struct{
	void (*appStart)(void);
	unsigned int (*appLoop)(unsigned int frameTimeMs);
	void (*appEnd)(void);
	unsigned int (*isAppRunning)(void);
} appDescriptor_t;

void appControllerStartApp(appDescriptor_t app);
unsigned int appControllerLoop(unsigned int frameTimeMs);
void appControllerStart(void);

#endif