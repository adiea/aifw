/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include "unistd.h"

#include "system.h"
#include "sysTick.h"
#include "lvgl.h"

#include "appcontroller.h"
#include "app_tyolo2.h"
#include "app_tyolo2_face.h"

int main(void)
{
	uint32_t lastTime, frameTime, currentTime, frameCount=0, lastSecondTime;
	
	systemInit();

	appControllerStart();
	
	//start the tiny yolo v2 app
	appControllerStartApp(appTYolo2_getDescriptor());

	lastTime = getMs();
	lastSecondTime = lastTime;
	gSysState.framesPerSecond = 0;
	
    while (1)
    {
		currentTime = getMs(); 
		frameTime = currentTime - lastTime;
		lastTime = currentTime;
		
		if(currentTime - lastSecondTime > 1000)
		{
			lastSecondTime = currentTime;
			gSysState.framesPerSecond = frameCount;
			frameCount = 0;
		}	
		
		if(frameTime)
		{
			lv_tick_inc(frameTime);
			lv_task_handler();
		}
		
		if(appControllerLoop(frameTime))
			++frameCount;
    }

    return 0;
}
