/*
Project: gitlab.com/adiea/aifw
License: MIT
*/
#include "ui.h"
#include "display.h"
#include "cam_sensor.h"

#include "appcontroller.h"
#include "app_tyolo2.h"
#include "app_tyolo2_face.h"

#include <stdio.h>

uint16_t gCurrentScreen = UI_SCREEN_MAIN;

lv_obj_t * gBkgImage = 0;

static lv_img_dsc_t gBkgImageDesc = {
  .header.always_zero = 0,
  .header.w = LV_HOR_RES_MAX,
  .header.h = LV_VER_RES_MAX,
  .header.cf = LV_IMG_CF_TRUE_COLOR,
  .data_size = LV_HOR_RES_MAX * LV_VER_RES_MAX * LV_COLOR_DEPTH / 8,
  .data = 0
};

static lv_obj_t *gCurrentAppControl;

static void onNewCameraFrame(uint32_t* buffer)
{
	if(UI_SCREEN_MAIN == gCurrentScreen)
	{
		if(gBkgImageDesc.data != (uint8_t *)buffer)
		{
			gBkgImageDesc.data = (uint8_t *)buffer;
			lv_obj_invalidate(gBkgImage);
		}
	}
}

static void onCurrentAppChanged(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_VALUE_CHANGED) {
        
        if(0 == lv_ddlist_get_selected(obj))
		{
			appControllerStartApp(appTYolo2_getDescriptor());
		}
		else if(1 == lv_ddlist_get_selected(obj))
		{
			appControllerStartApp(appTYolo2Face_getDescriptor());
		}
    }
}

void uiInit(void)
{
	/*Create a screen*/
	
	lv_obj_set_style(lv_scr_act(), &lv_style_plain);
	
	gBkgImage = lv_img_create(lv_scr_act(), NULL);
	lv_img_set_src(gBkgImage, &gBkgImageDesc);
	gBkgImageDesc.data = (uint8_t *)getDisplayFrontBuffer();
	setOnNewCamFrameAvailable(onNewCameraFrame);

	gCurrentAppControl = lv_ddlist_create(lv_scr_act(), NULL);
    lv_ddlist_set_options(gCurrentAppControl, "Detect Objects\nDetect Faces");
    lv_ddlist_set_fix_width(gCurrentAppControl, 150);
    lv_ddlist_set_draw_arrow(gCurrentAppControl, true);
    lv_obj_align(gCurrentAppControl, NULL, LV_ALIGN_IN_TOP_RIGHT, 0, 20);
    lv_obj_set_event_cb(gCurrentAppControl, onCurrentAppChanged);
}