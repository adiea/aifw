#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "unistd.h"
#include "app_tyolo2_face.h"
#include "apputils.h"
#include "ui.h"

#include "cam_sensor.h"
#include "lcd.h"
#include "display.h"
#include "system.h"
#include "ff.h"
#include "kpu.h"
#include "region_layer.h"

		/*** Config ***/
#define APP "yoloface:"

#define ANCHOR_NUM 5
/* All predicted bounding boxes that have an IOU value greater than 
   IOU_THRESHOLD with respect to the best bounding boxes will be removed. */
#define IOU_THRESHOLD 0.7
/* All predicted bounding boxes that have a detection probability 
   less than YOLO_NMS will be removed. */
#define NMS_THRESHOLD 0.3
#define YOLO_WIDTH 20 
#define YOLO_HEIGHT 15
#define YOLO_CHANNELS 30

static char gModelFileName[] = "/face.kmodel";
static float gYOLOAnchor[ANCHOR_NUM * 2] = {1.889,2.5245,  2.9465,3.94056, 3.99987,5.3658, 5.155437,6.92275, 6.718375,9.01025};

		/*** Application ***/
static unsigned int gIsInited = 0;

		/*** UI ***/
#define UI_NUM_AI_BOXES 10

static lv_obj_t* gUiStatusLabel = 0;

static ui_AIBoxes_t gUIAIBoxes[UI_NUM_AI_BOXES];

static char gStatusTextBuffer[32];

		/*** A I ***/
static uint8_t *gModelData = 0;

static kpu_model_context_t gModelContext;
static region_layer_t gFullyConnectedLayer;
static obj_info_t gYoloResults;

static volatile uint8_t g_ai_done_flag;

static int ai_done(void *ctx)
{
    g_ai_done_flag = 1;
    return 0;
}


/*** UI ***/

static void uiClearAIBoxes()
{
	for(uint8_t i = 0; i < UI_NUM_AI_BOXES; ++i)
	{
		lv_obj_set_hidden(gUIAIBoxes[i].textlabel, 1);
		lv_obj_set_hidden(gUIAIBoxes[i].line, 1);
	}
}

static void drawbox(obj_info_t *obj_info, uint32_t index, uint16_t color)
{
	if(!obj_info)
		return;
    
	uint32_t x1 = obj_info->obj[index].x1;
    uint32_t y1 = obj_info->obj[index].y1;
    uint32_t x2 = obj_info->obj[index].x2;
    uint32_t y2 = obj_info->obj[index].y2;
	
	if (x1 > 319) x1 = 319;
    if (x2 > 319) x2 = 319;
    if (y1 > 239) y1 = 239;
    if (y2 > 239) y2 = 239;

   uiDrawAIBox(&(gUIAIBoxes[index]), x1, y1, x2, y2, color, "");
}

static void appStart(void)
{
	/* load model from file */
	if(FR_OK != loadModelFile(gModelFileName, &gModelData))
		return;
	
	/* init kpu */
    if (kpu_load_kmodel(&gModelContext, gModelData) != 0)
    {
        printf(APP"model init error\n");
		return;
    }
	
	gFullyConnectedLayer.anchor_number = ANCHOR_NUM;
    gFullyConnectedLayer.anchor = gYOLOAnchor;
    gFullyConnectedLayer.threshold = IOU_THRESHOLD;
    gFullyConnectedLayer.nms_value = NMS_THRESHOLD;
	
	gYoloResults.obj = (yolo_result_t*)malloc(sizeof(yolo_result_t) * UI_NUM_AI_BOXES);
	gYoloResults.capacity = UI_NUM_AI_BOXES;
	
    region_layer_init(&gFullyConnectedLayer, YOLO_WIDTH, YOLO_HEIGHT, YOLO_CHANNELS, LCD_Y_MAX, LCD_X_MAX);
	
	/*** UI ***/
	
	/*Create anew style*/
	static lv_style_t style_txt;
	lv_style_copy(&style_txt, &lv_style_plain);
	style_txt.text.font = &lv_font_roboto_16;
	style_txt.text.letter_space = 2;
	style_txt.text.line_space = 1;
	style_txt.text.color = (lv_color_t)(uint16_t)0xFFFF;
	
	gUiStatusLabel = lv_label_create(lv_scr_act(), NULL);
	lv_obj_set_pos(gUiStatusLabel, 5, 200);
	lv_obj_set_style(gUiStatusLabel, &style_txt);
	
	//create ai boxes
	for(uint8_t i = 0; i < UI_NUM_AI_BOXES; ++i)
	{
		gUIAIBoxes[i].textlabel = lv_label_create(lv_scr_act(), NULL);
		gUIAIBoxes[i].line = lv_line_create(lv_scr_act(), NULL);
	}
	uiClearAIBoxes();	
	
	gIsInited = 1;
}

static void appEnd(void)
{
	lv_obj_del(gUiStatusLabel);
	for(uint8_t i = 0; i < UI_NUM_AI_BOXES; ++i)
	{
		lv_obj_del(gUIAIBoxes[i].textlabel);
		lv_obj_del(gUIAIBoxes[i].line);
	}
	
	free(gYoloResults.obj);
	gYoloResults.obj = 0;
	
	region_layer_deinit(&gFullyConnectedLayer);
	kpu_model_free(&gModelContext);
	if(gModelData)
	{
		free(gModelData);
		gModelData = 0;
	}
	gIsInited = 0;
}

static unsigned int appLoop(unsigned int frameTime)
{
	uint16_t numObjectsDetected=0;
	size_t networkOutputSize;
	
	if(isCameraFrameAvailable())
	{
		/* start to calculate */
		kpu_run_kmodel(&gModelContext, getAIBuffer(0), DMAC_CHANNEL5, ai_done, NULL);
		while(!g_ai_done_flag);
		g_ai_done_flag = 0;			
	
		kpu_get_output(&gModelContext, 0, &(gFullyConnectedLayer.input), &networkOutputSize);

		/* start region layer */
		numObjectsDetected = region_layer_run(&gFullyConnectedLayer, &gYoloResults);
		
		/* clear the previous detected objects*/
		uiClearAIBoxes();
		
		/* filter through the relevant objects */
		for (uint32_t objCnt = 0; objCnt < numObjectsDetected; objCnt++)
        {
			yolo_result_t *result = &(gYoloResults.obj[objCnt]);
			uiDrawAIBox(&(gUIAIBoxes[objCnt]), result->x1, result->y1, result->x2, result->y2, GREEN, "");
        }
		
		sprintf(gStatusTextBuffer, "[YOLO FACE] Ob:%d | FPS:%d", numObjectsDetected, gSysState.framesPerSecond);
		lv_label_set_text(gUiStatusLabel, gStatusTextBuffer);
		
		displayDraw(frameTime);

		return 1;
	}
	return 0;
}

static unsigned int isAppRunning(void)
{
	return gIsInited;
}

appDescriptor_t appTYolo2Face_getDescriptor(void)
{
	appDescriptor_t app;
	app.appStart = appStart;
	app.appLoop = appLoop;
	app.appEnd = appEnd;
	app.isAppRunning = isAppRunning;
	return app;
}