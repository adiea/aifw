#include <stdio.h>
#include <stdlib.h>
#include "apputils.h"

FRESULT loadModelFile(char* modelFileName, uint8_t ** modelData)
{
	FRESULT fr;
	FILINFO fno;
	FIL file;

    fr = f_stat(modelFileName, &fno);
    switch (fr) 
	{
    case FR_OK:
		*modelData = (uint8_t*)malloc(fno.fsize);
		
		if(*modelData)
		{
			if ((fr = f_open(&file, modelFileName, FA_READ)) != FR_OK) 
			{
				printf("open model file %s err %d\n", modelFileName, fr);
			}
			else
			{
				UINT bytesRead=0;
				f_read(&file, *modelData, fno.fsize, &bytesRead);
				if(bytesRead != fno.fsize)
				{
					printf("WW only read %d bytes from %s\n", bytesRead, modelFileName);
				}
				
				f_close(&file);
			}
		}
		
        break;

    case FR_NO_FILE:
        printf("Fstat: File %s does not exist.\n", modelFileName);
        break;

    default:
        printf("Fstat: error occured for file %s: (%d)\n", modelFileName, fr);
    }
	
	return fr;
}

void uiDrawAIBox(ui_AIBoxes_t *box, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint16_t color, const char* textCaption)
{
	if(!box)
		return;
	
	if (x1 > 319) x1 = 319;
    if (x2 > 319) x2 = 319;
    if (y1 > 239) y1 = 239;
    if (y2 > 239) y2 = 239;
	
	lv_obj_set_hidden(box->textlabel, 0);
	lv_obj_set_hidden(box->line, 0);
	
	lv_style_copy(&box->style, &lv_style_plain);
	box->style.line.color = (lv_color_t)(color);
	box->style.text.color = (lv_color_t)(color);
	box->style.line.width = 2;
		
	lv_label_set_text(box->textlabel, textCaption);
	
	lv_obj_set_pos(box->textlabel, x1 + 10, y2 - 20);
	
	lv_obj_set_style(box->textlabel, &box->style);
	
	box->line_points[0].x = x1, box->line_points[0].y = y1;
	box->line_points[1].x = x2, box->line_points[1].y = y1;
	box->line_points[2].x = x2, box->line_points[2].y = y2;
	box->line_points[3].x = x1, box->line_points[3].y = y2;
	box->line_points[4].x = x1, box->line_points[4].y = y1;

	lv_line_set_style(box->line, LV_LINE_STYLE_MAIN, &box->style);
	lv_line_set_points(box->line, box->line_points, 5);
	
	lv_obj_invalidate(box->textlabel);
	lv_obj_invalidate(box->line);
}