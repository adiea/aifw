
#include <stdio.h>
#include "appcontroller.h"
#include "ui.h"
#include "jsmn.h"

appDescriptor_t gCurrentApp;

void appControllerStartApp(appDescriptor_t app)
{
	if(gCurrentApp.isAppRunning)
	{
		if(gCurrentApp.isAppRunning())
		{
			gCurrentApp.appEnd();
		}
		if(gCurrentApp.isAppRunning())
		{
			printf("EE appStart(): current app hanging\n");
		}
	}
	
	gCurrentApp = app;
	
	if(gCurrentApp.appStart)
		gCurrentApp.appStart();
}

void appControllerStart(void)
{
	gCurrentApp.appStart = 0;
	gCurrentApp.appLoop = 0;
	gCurrentApp.appEnd = 0;
	gCurrentApp.isAppRunning = 0;
		
	uiInit();
}

unsigned int appControllerLoop(unsigned int frameTimeMs)
{
	if(gCurrentApp.isAppRunning && gCurrentApp.isAppRunning())
	{
		return gCurrentApp.appLoop(frameTimeMs);
	}
	return 0;
}