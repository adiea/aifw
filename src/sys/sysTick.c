/*
Project: gitlab.com/adiea/aifw
License: MIT
*/
#include "timer.h"

volatile uint32_t gElapsedMs = 0;

static int timer_callback(void *ctx) 
{
	++gElapsedMs;
	return 0;
}

uint32_t getMs(void)
{
	return gElapsedMs;
}

void sysTickInit(void)
{
	timer_init(0);
    timer_set_interval(0, 0, 1000000);
    /* Set timer callback function */
    timer_irq_register(0, 0, 0, 1, timer_callback, 0);
    /* Enable timer */
	timer_set_enable(0, 0, 1);
	
}