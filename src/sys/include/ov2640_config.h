/*
 * This file is part of the OpenMV project.
 * Copyright (c) 2013/2014 Ibrahim Abdelkader <i.abdalkader@gmail.com>
 * This work is licensed under the MIT license, see the file LICENSE for details.
 *
 * OV2640 driver.
 *
 */
// Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef _OV2640_CONF_
#define _OV2640_CONF_

#include "ov2640_regs.h"

const uint8_t ov2640_config_default[][2]=
{
#if 0	
	{BANK_SEL, BANK_DSP},
    {0x2c, 0xff}, //reserved ??
    {0x2e, 0xdf}, //reserved ??
    
	{BANK_SEL, BANK_SENSOR},
    {0x3c, 0x32}, //reserved ??
    {CLKRC, 0x00}, //clk prescaler = 1
    {COM2, COM2_OUT_DRIVE_3x}, //output drive select 2x (error in datasheet?)
    {REG04, REG04_DEFAULT},//bit7 horizontal mirror, bit 6 vertical flip
    {COM8, COM8_DEFAULT | COM8_BNDF_EN | COM8_AGC_EN | COM8_AEC_EN}, //banding filter selection exposure time min 1/120s, auto AGC, auto exposure
    {COM9, COM9_AGC_SET(COM9_AGC_GAIN_8x)}, //agc ceiling = 8x, 
    {0x2c, 0x0c}, //reserved ??
    {0x33, 0x78}, //reserved ??
    {0x3a, 0x33}, //reserved ??
    {0x3b, 0xfb}, //reserved ??
    {0x3e, 0x00}, //reserved ??
    {0x43, 0x11}, //reserved ??
    {0x16, 0x10}, //reserved ??
    {0x39, 0x92}, //reserved ??
    {0x35, 0xda}, //reserved ??
    {0x22, 0x1a}, //reserved ??
    {0x37, 0xc3}, //reserved ??
    {0x23, 0x00}, //reserved ??
    {ARCOM2, 0xc0}, //zoom window horizontal start position 0
    {0x36, 0x1a}, //reserved ?? (+)
    {0x06, 0x88}, //reserved ??
    {0x07, 0xc0}, //reserved ??
    {COM4, 0x87}, 
    {0x0e, 0x41}, //reserved ??
    {0x4c, 0x00}, //reserved ??
    {COM19, 0x00}, //zoom window vertical start point 2LSBs
    {0x5b, 0x00}, //reserved ?? (+)
    {0x42, 0x03}, //reserved ?? (+)
    {0x4a, 0x81}, //reserved ??
    {0x21, 0x99}, //reserved ??
    {AEW, 0x40}, //luminance signal high range
    {AEB, 0x38}, //luminance signal low range
    {VV, VV_AGC_TH_SET(8,2)}, //luminance control step high and low
    {0x5c, 0x00}, //reserved ??
    {0x63, 0x00}, //reserved ??
    {FLL, 0x22}, //frame length adjustments LSBs. Each bit will add 1 horizontal line timing in frame
    {COM3, COM3_BAND_SET(COM3_BAND_50Hz)}, //banding manually set 50Hz
    {HISTO_LOW, 0x70}, //histogram algorithm low level
    {HISTO_HIGH, 0x80}, //histogram algorithm high level
    {0x7c, 0x05}, //reserved ?? 
    {0x20, 0x80}, //reserved ??
    {0x28, 0x30}, //reserved ??
    {0x6c, 0x00}, //reserved ??
    {0x6d, 0x80}, //reserved ??
    {0x6e, 0x00}, //reserved ??
    {0x70, 0x02}, //reserved ??
    {0x71, 0x94}, //reserved ??
    {0x73, 0xc1}, //reserved ??
    {0x3d, 0x34}, //reserved ??
    {0x5a, 0x57}, //reserved ??
    {COM7, COM7_RES_SVGA}, //resolution: SVGA (!)
    {HSTART, 0x11}, //horizontal window start bits 10:3
    {HSTOP, 0x43}, //horizontal window end bits 10:3
    {VSTART, 0x00}, //vertical window start bits 9:2, rest in 0x03 0b10 1:0
    {VSTOP, 0x4b}, //vertical window end bits 9:2 rest ion 0x03 3:2 (0b10)
    {REG32, 0x09}, // lsb horizontal window start end bits 2:0 0b001 / 0b001
    {0x37, 0xc0}, //reserved ??
    {BD50, 0xca}, //50Hz banding aec
    {BD60, 0xa8}, //60Hz banding aec
    {0x5a, 0x23}, //app note 5.3: banding filer 3 step for 50hz,4 step for 60hz (good for 30fps)
    {0x6d, 0x00}, //reserved ??
    {0x3d, 0x38}, //reserved ??
	
    {BANK_SEL, BANK_DSP},
    {0xe5, 0x7f}, //reserved ??
    {MC_BIST, MC_BIST_RESET | MC_BIST_BOOT_ROM_SEL}, //mcu reset, boot rom select
    {0x41, 0x24}, //reserved ??
    {RESET, RESET_JPEG | RESET_DVP}, //reset jpeg unit
    {0x76, 0xff}, //reserved ??
    {0x33, 0xa0}, //reserved ??
    {0x42, 0x20}, //reserved ??
    {0x43, 0x18}, //reserved ??
    {0x4c, 0x00}, //reserved ??
    {CTRL3, CTRL3_DEFAULT | CTRL3_BPC_EN | CTRL3_WPC_EN /*| 0x05*/}, //enable BPC enable WPC
    {0x88, 0x3f}, //reserved ??
    {0xd7, 0x03},
    {0xd9, 0x10},
    {R_DVP_SP, R_DVP_SP_AUTO_MODE | R_DVP_SP_DEFAULT}, //DVP speed auto
    {0xc8, 0x08},
    {0xc9, 0x80},
    {BPADDR, 0x00}, //SDE indirect register access address/data 
    {BPDATA, 0x00}, 
    {BPADDR, 0x03}, //SDE indirect register access address/data 
    {BPDATA, 0x48}, {BPDATA, 0x48},
    {BPADDR, 0x08}, //SDE indirect register access address/data 
    {BPDATA, 0x20}, {BPDATA, 0x10}, {BPDATA, 0x0e},
    {0x90, 0x00},
    {0x91, 0x0e}, {0x91, 0x1a}, {0x91, 0x31}, {0x91, 0x5a}, 
	{0x91, 0x69}, {0x91, 0x75}, {0x91, 0x7e}, {0x91, 0x88}, 
	{0x91, 0x8f}, {0x91, 0x96}, {0x91, 0xa3}, {0x91, 0xaf}, 
	{0x91, 0xc4}, {0x91, 0xd7}, {0x91, 0xe8}, {0x91, 0x20},
    {0x92, 0x00},
    {0x93, 0x06}, {0x93, 0xe3}, {0x93, 0x05}, {0x93, 0x05},
    {0x93, 0x00}, {0x93, 0x04}, {0x93, 0x00}, {0x93, 0x00},
    {0x93, 0x00}, {0x93, 0x00}, {0x93, 0x00}, {0x93, 0x00},
    {0x93, 0x00},
    {0x96, 0x00},
    {0x97, 0x08}, {0x97, 0x19}, {0x97, 0x02}, {0x97, 0x0c},
    {0x97, 0x24}, {0x97, 0x30}, {0x97, 0x28}, {0x97, 0x26},
    {0x97, 0x02}, {0x97, 0x98}, {0x97, 0x80}, {0x97, 0x00},
    {0x97, 0x00},
    {CTRL1, 0xed},
    {0xa4, 0x00},
    {0xa8, 0x00},
    {0xc5, 0x11},
    {0xc6, 0x51},
    {0xbf, 0x80},
    //{RES_AWB, RES_AWB_SIMPLE_AWB}, //simple AWB selected, see section 6.4 of app note
	{RES_AWB, RES_AWB_ON}, //simple AWB selected, see section 6.4 of app note
    {0xb6, 0x66},
    {0xb8, 0xa5},
    {0xb7, 0x64},
    {0xb9, 0x7c},
    {0xb3, 0xaf},
    {0xb4, 0x97},
    {0xb5, 0xff},
    {0xb0, 0xc5},
    {0xb1, 0x94},
    {0xb2, 0x0f},
    {0xc4, 0x5c},
    {HSIZE8, 0x64},
    {VSIZE8, 0x4b},
    {SIZEL, 0x00},
    {CTRL2, 0x3d},
    {CTRLI, 0x00},
    {HSIZE, 0xc8},
    {VSIZE, 0x96},
    {XOFFL, 0x00},
    {YOFFL, 0x00},
    {VHYX, 0x00},
    {ZMOW, 0xc8},
    {ZMOH, 0x96},
    {ZMHH, 0x00},
    {R_DVP_SP, R_DVP_SP_DEFAULT},
    {CTRL1, 0xed},
    {0x7f, 0x00},
    {IMAGE_MODE, IMAGE_MODE_RGB565},//image mode RGB565
    {0xe5, 0x1f},
    {0xe1, 0x67},
    {RESET, 0x00}, //reset nothing ?
    {0xdd, 0x7f},
    {R_BYPASS, R_BYPASS_DSP_EN},
    
	{BANK_SEL, BANK_DSP},
    {RESET, RESET_DVP},
    {ZMOW, 0x50},
    {ZMOH, 0x3c},
    {ZMHH, 0x00},
    {RESET, 0x00},
    {0x00, 0x00}
#endif

{0xff, 0x01},
    {0x12, 0x80},
    {0xff, 0x00},
    {0x2c, 0xff},
    {0x2e, 0xdf},
    {0xff, 0x01},
    {0x3c, 0x32},
    {0x11, 0x00},
    {0x09, 0x02},
/*
register 0x04: bit7 horizontal mirror, bit 6 vertical flip
*/
    {0x04, 0x08},//
//#else
//    {0x04, 0x58},//
//#endif
    {0x13, 0xe5},
    {0x14, 0x48},
    {0x2c, 0x0c},
    {0x33, 0x78},
    {0x3a, 0x33},
    {0x3b, 0xfb},
    {0x3e, 0x00},
    {0x43, 0x11},
    {0x16, 0x10},
    {0x39, 0x92},
    {0x35, 0xda},
    {0x22, 0x1a},
    {0x37, 0xc3},
    {0x23, 0x00},
    {0x34, 0xc0},
    {0x36, 0x1a},
    {0x06, 0x88},
    {0x07, 0xc0},
    {0x0d, 0x87},
    {0x0e, 0x41},
    {0x4c, 0x00},
    {0x48, 0x00},
    {0x5b, 0x00},
    {0x42, 0x03},
    {0x4a, 0x81},
    {0x21, 0x99},
    {0x24, 0x40},
    {0x25, 0x38},
    {0x26, 0x82},
    {0x5c, 0x00},
    {0x63, 0x00},
    {0x46, 0x22},
    {0x0c, 0x3c},
    {0x61, 0x70},
    {0x62, 0x80},
    {0x7c, 0x05},
    {0x20, 0x80},
    {0x28, 0x30},
    {0x6c, 0x00},
    {0x6d, 0x80},
    {0x6e, 0x00},
    {0x70, 0x02},
    {0x71, 0x94},
    {0x73, 0xc1},
    {0x3d, 0x34},
    {0x5a, 0x57},
    {0x12, 0x40},
    {0x17, 0x11},
    {0x18, 0x43},
    {0x19, 0x00},
    {0x1a, 0x4b},
    {0x32, 0x09},
    {0x37, 0xc0},
    {0x4f, 0xca},
    {0x50, 0xa8},
    {0x5a, 0x23},
    {0x6d, 0x00},
    {0x3d, 0x38},
    {0xff, 0x00},
    {0xe5, 0x7f},
    {0xf9, 0xc0},
    {0x41, 0x24},
    {0xe0, 0x14},
    {0x76, 0xff},
    {0x33, 0xa0},
    {0x42, 0x20},
    {0x43, 0x18},
    {0x4c, 0x00},
    {0x87, 0xd5},
    {0x88, 0x3f},
    {0xd7, 0x03},
    {0xd9, 0x10},
    {0xd3, 0x82},
    {0xc8, 0x08},
    {0xc9, 0x80},
    {0x7c, 0x00},
    {0x7d, 0x00},
    {0x7c, 0x03},
    {0x7d, 0x48},
    {0x7d, 0x48},
    {0x7c, 0x08},
    {0x7d, 0x20},
    {0x7d, 0x10},
    {0x7d, 0x0e},
    {0x90, 0x00},
    {0x91, 0x0e},
    {0x91, 0x1a},
    {0x91, 0x31},
    {0x91, 0x5a},
    {0x91, 0x69},
    {0x91, 0x75},
    {0x91, 0x7e},
    {0x91, 0x88},
    {0x91, 0x8f},
    {0x91, 0x96},
    {0x91, 0xa3},
    {0x91, 0xaf},
    {0x91, 0xc4},
    {0x91, 0xd7},
    {0x91, 0xe8},
    {0x91, 0x20},
    {0x92, 0x00},
    {0x93, 0x06},
    {0x93, 0xe3},
    {0x93, 0x05},
    {0x93, 0x05},
    {0x93, 0x00},
    {0x93, 0x04},
    {0x93, 0x00},
    {0x93, 0x00},
    {0x93, 0x00},
    {0x93, 0x00},
    {0x93, 0x00},
    {0x93, 0x00},
    {0x93, 0x00},
    {0x96, 0x00},
    {0x97, 0x08},
    {0x97, 0x19},
    {0x97, 0x02},
    {0x97, 0x0c},
    {0x97, 0x24},
    {0x97, 0x30},
    {0x97, 0x28},
    {0x97, 0x26},
    {0x97, 0x02},
    {0x97, 0x98},
    {0x97, 0x80},
    {0x97, 0x00},
    {0x97, 0x00},
    {0xc3, 0xed},
    {0xa4, 0x00},
    {0xa8, 0x00},
    {0xc5, 0x11},
    {0xc6, 0x51},
    {0xbf, 0x80},
    {0xc7, 0x10},
    {0xb6, 0x66},
    {0xb8, 0xa5},
    {0xb7, 0x64},
    {0xb9, 0x7c},
    {0xb3, 0xaf},
    {0xb4, 0x97},
    {0xb5, 0xff},
    {0xb0, 0xc5},
    {0xb1, 0x94},
    {0xb2, 0x0f},
    {0xc4, 0x5c},
    {0xc0, 0x64},
    {0xc1, 0x4b},
    {0x8c, 0x00},
    {0x86, 0x3d},
    {0x50, 0x00},
    {0x51, 0xc8},
    {0x52, 0x96},
    {0x53, 0x00},
    {0x54, 0x00},
    {0x55, 0x00},
    {0x5a, 0xc8},
    {0x5b, 0x96},
    {0x5c, 0x00},
    {0xd3, 0x02},
    {0xc3, 0xed},
    {0x7f, 0x00},
    {0xda, 0x08},//
    {0xe5, 0x1f},
    {0xe1, 0x67},
    {0xe0, 0x00},
    {0xdd, 0x7f},
    {0x05, 0x00},
    {0xff, 0x00},
    {0xe0, 0x04},
    {0x5a, 0x50},
    {0x5b, 0x3c},
    {0x5c, 0x00},
    {0xe0, 0x00},
    {0x00, 0x00}

	
};

const  uint8_t ov2640_settings_to_cif[][2] = {
    {BANK_SEL, BANK_SENSOR},
    {COM7, COM7_RES_CIF},

    //Set the sensor output window
    {COM1, 0x0A},
    {REG32, REG32_CIF},
    {HSTART, 0x11},
    {HSTOP, 0x43},
    {VSTART, 0x00},
    {VSTOP, 0x25},

    {CLKRC, 0x01},
    {BD50, 0xca},
    {BD60, 0xa8},
    {0x5a, 0x23},
    {0x6d, 0x00},
    {0x3d, 0x38},
    {0x39, 0x92},
    {0x35, 0xda},
    {0x22, 0x1a},
    {0x37, 0xc3},
    {0x23, 0x00},
    {ARCOM2, 0xc0},
    {0x06, 0x88},
    {0x07, 0xc0},
    {COM4, 0x87},
    {0x0e, 0x41},
    {0x4c, 0x00},
    {BANK_SEL, BANK_DSP},
    {RESET, RESET_DVP},

    //Set the sensor resolution (UXGA, SVGA, CIF)
    {HSIZE8, 0x32},
    {VSIZE8, 0x25},
    {SIZEL, 0x00},

    //Set the image window size >= output size
    {HSIZE, 0x64},
    {VSIZE, 0x4a},
    {XOFFL, 0x00},
    {YOFFL, 0x00},
    {VHYX, 0x00},
    {TEST, 0x00},

    {CTRL2, CTRL2_DCW_EN | 0x1D},
    {CTRLI, CTRLI_LP_DP | 0x00},
    {R_DVP_SP, 0x82},
    {0, 0}
};

const  uint8_t ov2640_settings_to_svga[][2] = {
    {BANK_SEL, BANK_SENSOR},
    {COM7, COM7_RES_SVGA},

    //Set the sensor output window
    {COM1, 0x0A},
    {REG32, REG32_SVGA},
    {HSTART, 0x11},
    {HSTOP, 0x43},
    {VSTART, 0x00},
    {VSTOP, 0x4b},

    {CLKRC, 0x01},
    {0x37, 0xc0},
    {BD50, 0xca},
    {BD60, 0xa8},
    {0x5a, 0x23},
    {0x6d, 0x00},
    {0x3d, 0x38},
    {0x39, 0x92},
    {0x35, 0xda},
    {0x22, 0x1a},
    {0x37, 0xc3},
    {0x23, 0x00},
    {ARCOM2, 0xc0},
    {0x06, 0x88},
    {0x07, 0xc0},
    {COM4, 0x87},
    {0x0e, 0x41},
    {0x42, 0x03},
    {0x4c, 0x00},
    {BANK_SEL, BANK_DSP},
    {RESET, RESET_DVP},

    //Set the sensor resolution (UXGA, SVGA, CIF)
    {HSIZE8, 0x64},
    {VSIZE8, 0x4B},
    {SIZEL, 0x00},

    //Set the image window size >= output size
    {HSIZE, 0xC8},
    {VSIZE, 0x96},
    {XOFFL, 0x00},
    {YOFFL, 0x00},
    {VHYX, 0x00},
    {TEST, 0x00},

    {CTRL2, CTRL2_DCW_EN | 0x1D},
    {CTRLI, CTRLI_LP_DP | 0x00},
    {R_DVP_SP, 0x80},
    {0, 0}
};

const  uint8_t ov2640_settings_to_uxga[][2] = {
    {BANK_SEL, BANK_SENSOR},
    {COM7, COM7_RES_UXGA},

    //Set the sensor output window
    {COM1, 0x0F},
    {REG32, REG32_UXGA},
    {HSTART, 0x11},
    {HSTOP, 0x75},
    {VSTART, 0x01},
    {VSTOP, 0x97},

    {CLKRC, 0x01},
    {0x3d, 0x34},
    {BD50, 0xbb},
    {BD60, 0x9c},
    {0x5a, 0x57},
    {0x6d, 0x80},
    {0x39, 0x82},
    {0x23, 0x00},
    {0x07, 0xc0},
    {0x4c, 0x00},
    {0x35, 0x88},
    {0x22, 0x0a},
    {0x37, 0x40},
    {ARCOM2, 0xa0},
    {0x06, 0x02},
    {COM4, 0xb7},
    {0x0e, 0x01},
    {0x42, 0x83},
    {BANK_SEL, BANK_DSP},
    {RESET, RESET_DVP},

    //Set the sensor resolution (UXGA, SVGA, CIF)
    {HSIZE8, 0xc8},
    {VSIZE8, 0x96},
    {SIZEL, 0x00},

    //Set the image window size >= output size
    {HSIZE, 0x90},
    {VSIZE, 0x2c},
    {XOFFL, 0x00},
    {YOFFL, 0x00},
    {VHYX, 0x88},
    {TEST, 0x00},

    {CTRL2, CTRL2_DCW_EN | 0x1d},
    {CTRLI, 0x00},
    {R_DVP_SP, 0x82},
    {0, 0}
};

const  uint8_t ov2640_settings_jpeg3[][2] = {
    {BANK_SEL, BANK_DSP},
    {RESET, RESET_JPEG | RESET_DVP},
    {IMAGE_MODE, IMAGE_MODE_JPEG_EN | IMAGE_MODE_HREF_VSYNC},
    {0xD7, 0x03},
    {0xE1, 0x77},
    {0xE5, 0x1F},
    {0xD9, 0x10},
    {0xDF, 0x80},
    {0x33, 0x80},
    {0x3C, 0x10},
    {R_DVP_SP, 0x04},
    {0xEB, 0x30},
    {0xDD, 0x7F},
    {RESET, 0x00},
    {0, 0}
};

const uint8_t ov2640_settings_yuv422[][2] = {
    {BANK_SEL, BANK_DSP},
    {RESET, RESET_DVP},
    {IMAGE_MODE, IMAGE_MODE_YUV422},
    {0xD7, 0x01},
    {0xE1, 0x67},
    {RESET, 0x00},
    {0, 0},
};

const uint8_t ov2640_settings_rgb565[][2] = {
    {BANK_SEL, BANK_DSP},
    {RESET, RESET_DVP},
    {IMAGE_MODE, IMAGE_MODE_RGB565},
    {0xD7, 0x03},
    {0xE1, 0x77},
    {RESET, 0x00},
    {0, 0},
};

#define NUM_BRIGHTNESS_LEVELS (5)
const uint8_t brightness_regs[NUM_BRIGHTNESS_LEVELS + 1][5] = {
    {BPADDR, BPDATA, BPADDR, BPDATA, BPDATA },
    {0x00, 0x04, 0x09, 0x00, 0x00 }, /* -2 */
    {0x00, 0x04, 0x09, 0x10, 0x00 }, /* -1 */
    {0x00, 0x04, 0x09, 0x20, 0x00 }, /*  0 */
    {0x00, 0x04, 0x09, 0x30, 0x00 }, /* +1 */
    {0x00, 0x04, 0x09, 0x40, 0x00 }, /* +2 */
};

#define NUM_CONTRAST_LEVELS (5)
const uint8_t contrast_regs[NUM_CONTRAST_LEVELS + 1][7] = {
    {BPADDR, BPDATA, BPADDR, BPDATA, BPDATA, BPDATA, BPDATA },
    {0x00, 0x04, 0x07, 0x20, 0x18, 0x34, 0x06 }, /* -2 */
    {0x00, 0x04, 0x07, 0x20, 0x1c, 0x2a, 0x06 }, /* -1 */
    {0x00, 0x04, 0x07, 0x20, 0x20, 0x20, 0x06 }, /*  0 */
    {0x00, 0x04, 0x07, 0x20, 0x24, 0x16, 0x06 }, /* +1 */
    {0x00, 0x04, 0x07, 0x20, 0x28, 0x0c, 0x06 }, /* +2 */
};

#define NUM_SATURATION_LEVELS (5)
const uint8_t saturation_regs[NUM_SATURATION_LEVELS + 1][5] = {
    {BPADDR, BPDATA, BPADDR, BPDATA, BPDATA },
    {0x00, 0x02, 0x03, 0x28, 0x28 }, /* -2 */
    {0x00, 0x02, 0x03, 0x38, 0x38 }, /* -1 */
    {0x00, 0x02, 0x03, 0x48, 0x48 }, /*  0 */
    {0x00, 0x02, 0x03, 0x58, 0x58 }, /* +1 */
    {0x00, 0x02, 0x03, 0x68, 0x68 }, /* +2 */
};

#define NUM_SPECIAL_EFFECTS (7)
const uint8_t special_effects_regs[NUM_SPECIAL_EFFECTS + 1][5] = {
    {BPADDR, BPDATA, BPADDR, BPDATA, BPDATA },
    {0x00, 0X00, 0x05, 0X80, 0X80 }, /* no effect */
    {0x00, 0X40, 0x05, 0X80, 0X80 }, /* negative */
    {0x00, 0X18, 0x05, 0X80, 0X80 }, /* black and white */
    {0x00, 0X18, 0x05, 0X40, 0XC0 }, /* reddish */
    {0x00, 0X18, 0x05, 0X40, 0X40 }, /* greenish */
    {0x00, 0X18, 0x05, 0XA0, 0X40 }, /* blue */
    {0x00, 0X18, 0x05, 0X40, 0XA6 }, /* retro */
};

#define NUM_WB_MODES (4)
const uint8_t wb_modes_regs[NUM_WB_MODES + 1][3] = {
    {0XCC, 0XCD, 0XCE },
    {0x5E, 0X41, 0x54 }, /* sunny */
    {0x65, 0X41, 0x4F }, /* cloudy */
    {0x52, 0X41, 0x66 }, /* office */
    {0x42, 0X3F, 0x71 }, /* home */
};

#define NUM_AE_LEVELS (5)
const uint8_t ae_levels_regs[NUM_AE_LEVELS + 1][3] = {
    { AEW,  AEB,  VV  },
    {0x20, 0X18, 0x60 },
    {0x34, 0X1C, 0x00 },
    {0x3E, 0X38, 0x81 },
    {0x48, 0X40, 0x81 },
    {0x58, 0X50, 0x92 },
};

const uint8_t agc_gain_tbl[31] = {
    0x00, 0x10, 0x18, 0x30, 0x34, 0x38, 0x3C, 0x70, 0x72, 0x74, 0x76, 0x78, 0x7A, 0x7C, 0x7E, 0xF0,
    0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFA, 0xFB, 0xFC, 0xFD, 0xFE, 0xFF
};

#endif //_OV2640_CONF_