/*
Project: gitlab.com/adiea/aifw
License: MIT
*/
#ifndef DISPLAY__H
#define DISPLAY__H

#include "lvgl.h"

typedef enum
{
	aiBuffer_R = 0,
	aiBuffer_G,
	aiBuffer_B
} eAIBuffer_t;

uint32_t getAIBuffer(eAIBuffer_t buffer);

void displayInit(void);

uint32_t* getDisplayFrontBuffer(void);
uint32_t* getDisplayBackBuffer(void);

void displayDraw(uint32_t frameTime);

void setOnNewCamFrameAvailable(void (*fptr)(uint32_t*));

#endif /*DISPLAY__H*/