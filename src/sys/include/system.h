/*
Project: gitlab.com/adiea/aifw
License: MIT
*/
#ifndef SYSTEM__H
#define SYSTEM__H

typedef struct
{
	unsigned char sdcardMounted;
	unsigned int framesPerSecond;
	
} systemState_t;
extern systemState_t gSysState;

void systemInit(void);


#endif /*SYSTEM__H*/