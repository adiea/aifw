/*
Project: gitlab.com/adiea/aifw
License: MIT
*/

#ifndef SYSTICK__H
#define SYSTICK__H


void sysTickInit(void);
uint32_t getMs(void);

#endif /*SYSTICK__H*/