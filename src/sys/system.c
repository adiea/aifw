/*
Project: gitlab.com/adiea/aifw
License: MIT
*/
#include "system.h"
#include "fpioa.h"
#include "plic.h"
#include "sysctl.h"
#include "uarths.h"
#include "nt35310.h"
#include "sdcard.h"
#include "ff.h"
#include "display.h"
#include "cam_sensor.h"
#include <stdio.h>

#include "cam_sensor.h"
#include "sysTick.h"
#include "sysctl.h"

systemState_t gSysState;
FATFS gSDcard_fs;

/* SPI and DMAC usage
 *
 * SPI0 - LCD
 * SPI1 - SD card
 * SPI2 - unused
 * SPI3 - Flash
 *
 * DMAC Channel 0 - LCD
 * DMAC Channel 1 - SD card
 *
 */

static void io_mux_init(void)
{
	/* SD card */
    fpioa_set_function(27, FUNC_SPI1_SCLK);
    fpioa_set_function(28, FUNC_SPI1_D0);//mosi
    fpioa_set_function(26, FUNC_SPI1_D1); //miso
    fpioa_set_function(29, FUNC_GPIOHS7); //ss
	
    /* Init DVP IO map and function settings */
    fpioa_set_function(42, FUNC_CMOS_RST);
    fpioa_set_function(44, FUNC_CMOS_PWDN);
    fpioa_set_function(46, FUNC_CMOS_XCLK);
    fpioa_set_function(43, FUNC_CMOS_VSYNC);
    fpioa_set_function(45, FUNC_CMOS_HREF);
    fpioa_set_function(47, FUNC_CMOS_PCLK);
    fpioa_set_function(41, FUNC_SCCB_SCLK);
    fpioa_set_function(40, FUNC_SCCB_SDA);

    /* Init SPI IO map and function settings */
    fpioa_set_function(38, FUNC_GPIOHS0 + DCX_GPIONUM);
    fpioa_set_function(36, FUNC_SPI0_SS3);
    fpioa_set_function(39, FUNC_SPI0_SCLK);
    fpioa_set_function(37, FUNC_GPIOHS0 + RST_GPIONUM);

    sysctl_set_spi0_dvp_data(1);
		
	//touchscreen
    fpioa_set_function(30, FUNC_I2C0_SCLK);
    fpioa_set_function(31, FUNC_I2C0_SDA);
}

static void io_set_power(void)
{
    /* Set dvp and spi pin to 1.8V */
    sysctl_set_power_mode(SYSCTL_POWER_BANK6, SYSCTL_POWER_V18);
    sysctl_set_power_mode(SYSCTL_POWER_BANK7, SYSCTL_POWER_V18);
	
	//enable power for bank 2 (buttons and leds)
	//sysctl_set_power_mode(SYSCTL_POWER_BANK2, SYSCTL_POWER_V33);
}

void systemInit(void)
{
	FRESULT result;
	
	/* Set CPU and dvp clk */
    sysctl_pll_set_freq(SYSCTL_PLL0, 800000000UL);
    sysctl_pll_set_freq(SYSCTL_PLL1, 160000000UL);
    sysctl_pll_set_freq(SYSCTL_PLL2, 45158400UL);
    uarths_init();
	printf("[sys]UART inited\n");

    io_mux_init();
    io_set_power();
	printf("[sys]GPIO inited\n");
	
    plic_init();

	/* SD card init and mount */
	gSysState.sdcardMounted = 1;
	
    if (sd_init())
    {
        printf("[sys]fail to init SD card\n");
		gSysState.sdcardMounted = 0;
    }

	printf("[sys]SD Capacity:%ld MB\n", cardinfo.CardCapacity/1024/1024);
	//printf("[sys]SD BlockSize:%d\n", cardinfo.CardBlockSize);
	
	/* mount file system to SD card */
    if (result = f_mount(&gSDcard_fs, _T("0:"), 1) != FR_OK)
    {
        printf("[sys]fail to mount file system %d\n", result);
		gSysState.sdcardMounted = 0;
	}
	
	/* ls
	{
	FRESULT status;
    DIR dj;
    FILINFO fno;

    status = f_findfirst(&dj, &fno, _T("0:"), _T("*"));
    while (status == FR_OK && fno.fname[0])
    {
        if (fno.fattrib & AM_DIR)
            printf("dir:%s\r\n", fno.fname);
        else
            printf("file:%s\r\n", fno.fname);
        status = f_findnext(&dj, &fno);
    }
	f_closedir(&dj);
	}
	*/
	
	/*FRESULT result;
	FIL file;
	
	if ((result = f_open(&file, _T("0:/a.txt"), FA_CREATE_ALWAYS | FA_WRITE)) != FR_OK) 
    {
        printf("create file err %d\n", result);
    }
	else
	{
		char c='a';
		f_write(&file, &c, 1, NULL);
		f_close(&file);
	}*/
	
	if(gSysState.sdcardMounted == 1)
		printf("[sys]SD card inited\n");

	displayInit();
	cameraInit();
	cameraSetFrameBuffer(getDisplayBackBuffer());
	cameraSetAIBuffers(getAIBuffer(aiBuffer_R), getAIBuffer(aiBuffer_G), getAIBuffer(aiBuffer_B));
	
	//enable the interrupts and start the system    
	sysctl_enable_irq();
	cameraStart();	
	sysTickInit();
}