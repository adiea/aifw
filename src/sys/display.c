/*
Project: gitlab.com/adiea/aifw
License: MIT
*/

#include "display.h"
#include "ns2009.h"
#include "lcd.h"
#include "cam_sensor.h"
#include <stdio.h>

#define LCD_BYTES_PER_PIXEL 2

uint32_t g_lcd_gram0[LCD_X_MAX * LCD_Y_MAX * LCD_BYTES_PER_PIXEL / sizeof(uint32_t)] __attribute__((aligned(64)));
uint32_t g_lcd_gram1[LCD_X_MAX * LCD_Y_MAX * LCD_BYTES_PER_PIXEL / sizeof(uint32_t)] __attribute__((aligned(64)));
uint8_t  g_ai_buf[LCD_X_MAX * LCD_Y_MAX * 3] __attribute__((aligned(128)));

uint32_t *gScreenBuffer[] = {g_lcd_gram0, g_lcd_gram1};
volatile uint8_t gDisplayActiveBuffer = 0;

void (*gOnNewCamFrameAvailable)(uint32_t *) = 0;

void setOnNewCamFrameAvailable(void (*fptr)(uint32_t *))
{
	gOnNewCamFrameAvailable = fptr;
}

uint32_t getAIBuffer(eAIBuffer_t buffer)
{
	return (uint32_t)(g_ai_buf + LCD_X_MAX * LCD_Y_MAX * (uint8_t)buffer);
}

uint32_t* getDisplayFrontBuffer(void)
{
	return gScreenBuffer[gDisplayActiveBuffer];
}

uint32_t* getDisplayBackBuffer(void)
{
	return gScreenBuffer[gDisplayActiveBuffer ^ 0x01];
}

static void lvglGlue_displayFlush(lv_disp_drv_t * disp_drv, const lv_area_t * area, lv_color_t * color_map)
{
		(void)(disp_drv);
		//allow the camera to get another frame, this one was processed
		if(isCameraFrameAvailable())
		{
			//switch the active buffer
			gDisplayActiveBuffer ^= 0x01;
			
			cameraSetFrameBuffer(getDisplayBackBuffer());
			cameraFrameProcessed();
		}
	
		//lcd_draw_picture(0, 0, LCD_Y_MAX, LCD_X_MAX, drawBuffer);
		lcd_draw_picture_by_half(area->x1, area->y1, area->x2 - area->x1 + 1, area->y2 - area->y1 + 1, (uint16_t *)color_map);

		lv_disp_flush_ready(disp_drv);
}

static bool lvglGlue_tp_read(lv_indev_drv_t * drv, lv_indev_data_t *data)
{
	(void)(drv);
	static struct ts_ns2009_pdata_t *ts_ns2009_pdata = NULL;
	
	if(ts_ns2009_pdata == NULL)
	{
		ts_ns2009_pdata = ts_ns2009_probe();
		if (ts_ns2009_pdata == NULL)
			printf("ns1009 init error!\r\n");
	}
	
    ts_ns2009_poll(ts_ns2009_pdata);

    switch (ts_ns2009_pdata->event->type)
    {
    case TOUCH_BEGIN:
        data->point.x = ts_ns2009_pdata->event->x;
        data->point.y = ts_ns2009_pdata->event->y;
        data->state = LV_INDEV_STATE_PR;
        return false;

    case TOUCH_MOVE:
        data->point.x = ts_ns2009_pdata->event->x;
        data->point.y = ts_ns2009_pdata->event->y;
        data->state = LV_INDEV_STATE_PR;
        return false;

    case TOUCH_END:
        data->point.x = ts_ns2009_pdata->event->x;
        data->point.y = ts_ns2009_pdata->event->y;
        data->state = LV_INDEV_STATE_REL;
        return false;

    default:
        break;
    }

    return false;
}

void displayInit(void)
{
	static lv_disp_buf_t disp_buf;
    static lv_color_t buf_1[LV_HOR_RES_MAX * LV_VER_RES_MAX];            /*A screen sized buffer*/
    static lv_color_t buf_2[LV_HOR_RES_MAX * LV_VER_RES_MAX];            /*An other screen sized buffer*/
    lv_disp_buf_init(&disp_buf, buf_1, buf_2, LV_HOR_RES_MAX * LV_VER_RES_MAX);   /*Initialize the display buffer*/

	lcd_init();
	lcd_set_direction(DIR_YX_LRUD);
    lcd_clear(WHITE);

	lv_init();	
	
	lv_disp_drv_t disp_drv;    /*Descriptor of a display driver*/
	lv_disp_drv_init(&disp_drv); /*Basic initialization*/
	disp_drv.flush_cb = lvglGlue_displayFlush; 
	
	/*Set the resolution of the display*/
    disp_drv.hor_res = 320;
    disp_drv.ver_res = 240;
	
	/*Set a display buffer*/
    disp_drv.buffer = &disp_buf;
	
	/*Register the driver*/
	lv_disp_drv_register(&disp_drv);
	
	lv_indev_drv_t indev_drv;
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read_cb = lvglGlue_tp_read;
    lv_indev_drv_register(&indev_drv);
	printf("LVGL inited\n");
}

void displayDraw(uint32_t frameTime)
{
	if(isCameraFrameAvailable())
	{
		if(gOnNewCamFrameAvailable)
		{
			gOnNewCamFrameAvailable(getDisplayFrontBuffer());
		}
	}
}
