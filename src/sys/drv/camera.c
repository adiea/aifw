/*
Project: gitlab.com/adiea/aifw
License: MIT
*/
#include "cam_sensor.h"
#include "ov2640.h"
#include "dvp.h"
#include "plic.h"
#include <stdio.h>

const int cam_resolution[][2] = {
    { 160, 120 }, /* QQVGA */
    { 128, 160 }, /* QQVGA2*/
    { 176, 144 }, /* QCIF  */
    { 240, 176 }, /* HQVGA */
    { 320, 240 }, /* QVGA  */
    { 400, 296 }, /* CIF   */
    { 640, 480 }, /* VGA   */
    { 800, 600 }, /* SVGA  */
    { 1024, 768 }, /* XGA  */
    { 1280, 1024 }, /* SXGA  */
    { 1600, 1200 }, /* UXGA  */
    { 2048, 1536 }, /* QXGA  */
};

cam_sensor_t gCameraSensor;
uint16_t* gFrameBufferPtr = 0;

volatile uint8_t g_camFrameAvailable = 0;

static int on_irq_dvp(void* ctx)
{
	uint32_t i;
	uint16_t change;
    if (dvp_get_interrupt(DVP_STS_FRAME_FINISH))
    {
		for(i=0; i < 240*320; i += 2)
		{
			change = gFrameBufferPtr[i];
			gFrameBufferPtr[i] = gFrameBufferPtr[i+1];
			gFrameBufferPtr[i+1] = change;
		}
		
		g_camFrameAvailable = 1;
		dvp_clear_interrupt(DVP_STS_FRAME_FINISH);
    }
    else
    {
        if(g_camFrameAvailable == 0)
		{
            dvp_start_convert();
		}
        dvp_clear_interrupt(DVP_STS_FRAME_START);
    }

    return 0;
}

void cameraInit(void)
{
    /* DVP init */
    printf("DVP init\n");
   
    dvp_init(8);
    dvp_set_xclk_rate(24000000);
    
	dvp_enable_burst();
	
	dvp_set_output_enable(DVP_OUTPUT_AI , 1);
	dvp_set_output_enable(DVP_OUTPUT_DISPLAY , 1);
    dvp_set_image_format(DVP_CFG_RGB_FORMAT);
    dvp_set_image_size(320, 240);

	/* DVP interrupt config */
	dvp_config_interrupt(DVP_CFG_START_INT_ENABLE | DVP_CFG_FINISH_INT_ENABLE, 0);
    dvp_disable_auto();
    plic_set_priority(IRQN_DVP_INTERRUPT, 1);
    plic_irq_register(IRQN_DVP_INTERRUPT, on_irq_dvp, NULL);
    plic_irq_enable(IRQN_DVP_INTERRUPT);
	
	gCameraSensor.slv_addr = OV2640_ADDR;
	ov2640_init(&gCameraSensor);
	gCameraSensor.reset(&gCameraSensor);
	gCameraSensor.set_vflip(&gCameraSensor, 1);
	gCameraSensor.set_hmirror(&gCameraSensor, 1);
	printf("CAM inited\n");
}

void cameraStart(void)
{
    dvp_clear_interrupt(DVP_STS_FRAME_START | DVP_STS_FRAME_FINISH);
    dvp_config_interrupt(DVP_CFG_START_INT_ENABLE | DVP_CFG_FINISH_INT_ENABLE, 1);
	printf("CAM started\n");
}

uint8_t isCameraFrameAvailable(void)
{
	return g_camFrameAvailable;
}

void cameraFrameProcessed(void)
{
	g_camFrameAvailable = 0;
}

void cameraSetAIBuffers(uint32_t rBuffer, uint32_t gBuffer, uint32_t bBuffer)
{
	dvp_set_ai_addr(rBuffer, gBuffer, bBuffer);
}

void cameraSetFrameBuffer(uint32_t* frameBuffer)
{
	gFrameBufferPtr = (uint16_t*)frameBuffer;
	dvp_set_display_addr((uint32_t)frameBuffer);
}

cam_sensor_t* getCameraSensor()
{
	return &gCameraSensor;	
}